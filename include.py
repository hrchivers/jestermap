'''
Created on 11 Jan 2020

@author: Howard Chivers
Copyright (c) 2020, Howard Chivers
All rights reserved.
See docs for licence

'''

import datetime

DEBUG = False

# if LOG_L:AYER True then all boats logs written to a single layer
# else boat logs written to separate layer for each boat
# WARNING if separate layers are used - Google My Maps may limit the total number of layers 10
# (2 layers system reserved - event and tracks)
LOG_LAYER   = True

# Files
TRACK_FILE  = 'jester_tracks.csv'
CONFIG_FILE = 'jester_config.csv'
BOAT_FILE   = 'jester_boats.csv'
KML_FILE    = 'doc.kml'
LOG_FILE    = 'jester.log'
ICON_DIR    = 'images'

TEXT_ESCAPES    = (('<', '&lt;'), ('>', '&gt;'), ('&', '&amp;'))

COLOURS         = {'blue': 'FFFF0000', 'cyan': 'FFFFFF00', 'green': 'FF00FF00', 'pink': 'FFFF00FF', 'red': 'FF0000FF', 'yellow': 'FF00FFFF'}
ALL_COLOURS     = {**COLOURS, 'black': 'FF000000'}


# CSV Input Templates
# *******************

TEMPLATE_CONFIG = (('name', 'name', True, 'str'),
                   ('value', 'value', True, 'str'),
                   ('comment', 'comment', False, 'str'))
CONFIG_REQUIRED = []
CONFIG_DEFAULT = {'event_name': 'Jester Challenge', 'event_description': ' ', 'start_description': ' ', 'finish_description': ' ', 'start_end': [], 'finish_end': [], 'route_waymark': []}

TEMPLATE_BOATS  = (('boat', 'boat', True, 'str'),
                   ('sailor', 'sailor', True, 'str'),
                   ('type', 'type', False, 'str'))

TEMPLATE_TRACKS = (('boat', 'boat', True, 'str'),
                   ('date', 'date', True, 'date'),
                   ('time', 'time', False, 'time'),
                   ('position', 'position', True, 'position'),
                   ('status', 'status', False, 'str'),
                   ('log', 'log', False, 'str'))
TRACK_DEFAULTS = {'time': datetime.time(hour=12), 'log': '', 'status': ''}

XML_VERSION     = '<?xml version="1.0" encoding="UTF-8" ?>'
KML_NAMESPACES  = {'k_attrib': {'xmlns': 'http://earth.google.com/kml/2.0'}}


# Map Styles
# **********

JESTER_STYLES   = [{'k_attrib': {'id': 'eventLine'}, 'LineStyle': [{'k_value': {'color': 'FF00FFFF', 'width': 2}}]},
                   {'k_attrib': {'id': 'eventRoute'}, 'LineStyle': [{'k_value': {'color': 'FF000066', 'width': 2}}]}]

JESTER_STYLEMAP = []


for c in ALL_COLOURS:
    JESTER_STYLES.append({'k_attrib': {'id': 'track_{}'.format(c)}, 'LineStyle': [{'k_value': {'color': ALL_COLOURS[c], 'width': 2}}],
                          'IconStyle': [{'k_value': {'scale': 2, 'Icon': '<href>{}/sailing_{}.png</href>'.format(ICON_DIR, c)}}],
                          'LabelStyle': [{'k_value': {'scale': 0.9}}],
                          'BalloonStyle': [{'k_value': {'text': '<b>$[name]</b><br/>$[description]'}}]})
    JESTER_STYLES.append({'k_attrib': {'id': 'log_{}'.format(c)}, 'LineStyle': [{'k_value': {'color': ALL_COLOURS[c], 'width': 2}}],
                          'IconStyle': [{'k_value': {'scale': 1.5, 'Icon': '<href>{}/log_{}.png</href>'.format(ICON_DIR, c)},
                                         'hotSpot': [{'k_attrib': {'x': 0.3, 'y': 0, 'xunits': 'fraction', 'yunits': 'fraction'}}]}],
                          'LabelStyle': [{'k_value': {'scale': 0.0}}], 'BalloonStyle': [{'k_value': {'text': '<b>$[name]</b><br/>$[description]'}}]})

JESTER_STYLES.append({'k_attrib': {'id': 'Point_black'}, 'LineStyle': [{'k_value': {'color': 'FF000000', 'width': 2}}],
                      'IconStyle': [{'k_value': {'scale': 1.0, 'Icon': '<href>{}/circle_black.png</href>'.format(ICON_DIR)}}],
                      'LabelStyle': [{'k_value': {'scale': 0.0}}], 'BalloonStyle': [{'k_value': {'text': '<b>$[name]</b><br/>$[description]'}}]})

for c in COLOURS:
    JESTER_STYLES.append({'k_attrib': {'id': 'Point_{}'.format(c)}, 'LineStyle': [{'k_value': {'color': ALL_COLOURS[c], 'width': 2}}],
                          'IconStyle': [{'k_value': {'scale': 0.5, 'Icon': '<href>{}/circle_{}.png</href>'.format(ICON_DIR, c)}}],
                          'LabelStyle': [{'k_value': {'scale': 0.0}}], 'BalloonStyle': [{'k_value': {'text': '<b>$[name]</b><br/>$[description]'}}]})
    JESTER_STYLEMAP.append({'k_attrib': {'id': 'h_track_{}'.format(c)}, 'Pair': [{'k_value': {'key': 'normal', 'styleUrl': '#track_{}'.format(c)}}, {'k_value': {'key': 'highlight', 'styleUrl': '#track_black'}}]})
    JESTER_STYLEMAP.append({'k_attrib': {'id': 'h_log_{}'.format(c)}, 'Pair': [{'k_value': {'key': 'normal', 'styleUrl': '#log_{}'.format(c)}}, {'k_value': {'key': 'highlight', 'styleUrl': '#log_black'}}]})
    JESTER_STYLEMAP.append({'k_attrib': {'id': 'h_Point_{}'.format(c)}, 'Pair': [{'k_value': {'key': 'normal', 'styleUrl': '#Point_{}'.format(c)}}, {'k_value': {'key': 'highlight', 'styleUrl': '#Point_black'}}]})

JESTER_EVENT    = {'k_value': {'name': '%%event_name%%', 'event_description': '%%event_description%%'}}


# MAP KML Templates
# *****************

"""
Overlays cannot be used with My Maps - they throw an error, not just ignored, this was originally used to put the event name on the map

JESTER_COURSE    = {'k_value': {'name': '%%event_name%%', 'open': 0, 'event_description': '%%event_description%%'},
                   'ScreenOverlay': [{'k_value': {'name': 'Jester Challenge', 'Icon': '<href>{}/%%event_icon%%</href>'.format(ICON_DIR)},
                                      'overlayXY': [{'k_attrib': {'x': '0', 'y': '1', 'xunits': 'fraction', 'yunits': 'fraction'}}],
                                      'screenXY': [{'k_attrib': {'x': '0', 'y': '1', 'xunits': 'fraction', 'yunits': 'fraction'}}],
                                      'rotationXY': [{'k_attrib': {'x': '0', 'y': '0', 'xunits': 'fraction', 'yunits': 'fraction'}}],
                                      'sizeXY': [{'k_attrib': {'x': '0', 'y': '0', 'xunits': 'fraction', 'yunits': 'fraction'}}]}]}
"""

JESTER_COURSE    = {'k_value': {'name': '%%event_name%%'}}

JESTER_START    = {'k_value': {'name': 'Start Line', 'description': '%%start_description%%', 'styleUrl': '#eventLine'},
                   'LineString': [{'k_value': {'extrude': 1, 'tessellate': 1, 'coordinates': '%%start_end%%'}}]}
JESTER_FINISH   = {'k_value': {'name': 'Finish Line', 'description': '%%finish_description%%', 'styleUrl': '#eventLine'},
                   'LineString': [{'k_value': {'extrude': 1, 'tessellate': 1, 'coordinates': '%%finish_end%%'}}]}
JESTER_ROUTE    = {'k_value': {'name': 'Route - Great Cicle', 'styleUrl': '#eventRoute'},
                   'LineString': [{'k_value': {'extrude': 1, 'tessellate': 1, 'altitudeMode': 'clampToGround', 'coordinates': '%%route_waymark%%'}}]}
EVENT_CONFIG    = {'start_end': JESTER_START, 'finish_end': JESTER_FINISH, 'route_waymark': JESTER_ROUTE}

TRACKS_ROOT     = {'k_value': {'name': 'Tracks', 'open': 0}}
LOGS_ROOT       = {'k_value': {'name': 'Logs', 'open': 0, 'visibility': 1}}

BOAT_PLACE      = {'k_value': {'styleUrl': '#h_track_%%boat_colour%%', 'name': '%%boat_displayname%% %%last_date%% %%last_status%%', 'description': '%%boat_type%% (%%boat_sailor%%)<br/>%%last_log%%'},
                   'MultiGeometry': [{'Point': [{'k_value': {'coordinates': '%%last_position%%'}}],
                                      'LineString': [{'k_value': {'extrude': 1, 'tessellate': 1, 'altitudeMode': 'clampToGround', 'coordinates': '%%boat_track%%'}}]}]}
BOAT_FOLDER     = {'k_value': {'name': '%%boat_displayname%%', 'visibility': 1}}
LOG_PLACE       = {'k_value': {'styleUrl': '#h_log_%%boat_colour%%', 'name': '%%boat_displayname%% %%log_date%% %%log_status%%', 'description': '%%log_log%%', 'visibility': 1},
                   'Point': [{'k_value': {'coordinates': '%%log_position%%'}}]}
POINT_PLACE     = {'k_value': {'styleUrl': '#h_Point_%%boat_colour%%', 'name': '%%boat_displayname%% %%log_date%% %%log_status%%', 'visibility': 1},
                   'Point': [{'k_value': {'coordinates': '%%log_position%%'}}]}
CAMERA          = {'k_value': {'longitude': '%%camera_longitude%%', 'latitude': '%%camera_latitude%%', 'range': '%%camera_range%%', 'tilt': 20, 'heading': 0}}
