'''
Created on 13 Dec 2021

@author: Howard

very basic test of csv format conversions
uses file csv_test_csv in ref/test

'''

from utils import CSVRead, findData
import logging
import datetime
import sys

TEST_FILE = 'test_csv.csv'
TEST_DIR  = 'ref\\test'

TEMPLATE  = (('ref', 'ref', False, 'str'),
             ('date', 'date', False, 'date'),
             ('time', 'time', False, 'time'),
             ('position', 'position', False, 'position'),
             ('text', 'text', False, 'str'),
             ('end', 'end', False, 'str'))

'''
file info:
ref has comma separated field names,
if named the corresponding field value is used as a reference and must
be equal (or close) to any values in the same column until the next reference.

fields that are not checked against a ref will still be parsed

ref = reset removes any existing refs
'''

if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG, format="%(name)-12s %(levelname)-8s %(message)s")
    console = logging.StreamHandler(sys.stdout)
    print("csv test started at " + datetime.datetime.now().strftime("%d-%m-%Y %H:%M"))
    ref = {}

    for i, k in enumerate(CSVRead(findData(TEST_DIR), TEST_FILE, TEMPLATE)):
        if 'ref' in k and k['ref']:
            ref = {}
            if k['ref'] == 'reset':
                continue
            for field in k['ref'].split(','):
                if field not in k or not k[field]:
                    logging.error('Line {:d}: ref field ({}) not present'.format(i + 1, field))
                else:
                    ref[field] = k[field]
            print('{:d}\t new ref:'.format(i + 1), ref)
        else:
            print('{:d}\t'.format(i + 1), k[field])
            for field in ref:
                if field not in k or not k[field]:
                    logging.error('Line {:d}: required field ({}) not present to test'.format(i + 1, field))
                elif field == 'position':
                    if abs(ref[field][0] - k[field][0]) > 1e-6 or abs(ref[field][1] - k[field][1]) > 1e-6:
                        logging.error('Line {:d}: failed - not close to test field: {}'.format(i + 1, field))
                elif ref[field] != k[field]:
                    logging.error('Line {:d}: failed test field: {}'.format(i + 1, field))

    print('done, total lines =', i + 1)
