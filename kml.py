'''
Created on 13 Jan 2020

@author: Howard Chivers
Copyright (c) 2020, Howard Chivers
All rights reserved.
See docs for licence

build and output kml (xml google map) files
'''

from include import KML_FILE, XML_VERSION, CAMERA, TEXT_ESCAPES
from utils import boundsof, outputCheck
from copy import deepcopy
import logging
import os
import math

_kmlLog = None


def getCamera(locations):
    ''' returns camera position node (LookAt) for given location list
    '''
    max_lat, min_lat, max_long, min_long, center = boundsof(locations)

    meta = {}
    meta['camera_range']     = 75000 * int(math.sqrt(math.pow(max_lat - min_lat, 2) + math.pow(max_long - min_long, 2)))
    if meta['camera_range'] < 50000:
        meta['camera_range'] = 50000
    meta['camera_latitude']  = center[0]
    meta['camera_longitude'] = center[1]
    camera   = Kml(deepcopy(CAMERA))
    for k in meta:
        camera.substitute(k, meta[k])
    return camera


def escape(text):
    for sub in TEXT_ESCAPES:
        text = text.replace(*sub)
    return text


class Kml():
    '''
    maintains internal xml object with simple nested dict structure:
    name: [{k_attrib: {name:vale} k_value: { valuename: int, string or list}, name :[{} ... ]}...]

    values that are 'coordinates' are handled as a list of tuples, kept as floats in the tree

    kml templates are used to represent generic map objects. Templates have named substitution points
    %%<name>%%. They are first added to nodes in the tree then the specific data substituted. Scope of
    substitution (as well as node additions) controlled by cursor.
    '''

    def __init__(self, rnode):
        ''' namespaces (dict)     attribute list for kml root
        '''
        global _kmlLog
        self.root = {'kml': [deepcopy(rnode)]}
        self.setCursor(('kml',))
        if not _kmlLog:
            _kmlLog = logging.getLogger('main.kml')

    def setCursor(self, navlist):
        ''' set cursor to position in tree
            will go to first element if several have the same name
            returns the node for the current position
            navlist (list of str)    list of keys in descending order
        '''
        self.cursor = self.root
        for tag in navlist:
            self.cursor = self.cursor[tag][0]
        return self.cursor

    def addChild(self, cname, child, moveToChild=False):
        ''' cname (str)   name of child
            child (dict) child record, may be empty dictionary
            movetochild (bool) if true move cursor to new child

            (otherwise cursor is not moved)
        '''
        c = deepcopy(child)
        if cname in self.cursor:
            self.cursor[cname].append(c)
        else:
            self.cursor[cname] = [c]
        if moveToChild:
            self.cursor = c

    def _nodes(self, knode):
        ''' recurse through all nodes
            returns (node type, node dict)
        '''
        for nt in knode:
            if nt in ('k_attrib', 'k_value'):
                continue
            for child in knode[nt]:
                for cc in self._nodes(child):
                    yield cc
                yield nt, child

    def nodes(self, mtype=None, attrib=None, value=None):
        ''' traverse tree below cursor and return pointer to all nodes, or filtered as below
            mtype (str)  type of node to match
            attrib (str, value) name and optional value of attribute
            value  (str, value) name and optional value in node

            yields (node type, node dict)
        '''
        for ntype, node in self._nodes(self.cursor):
            if mtype and ntype != mtype:
                continue
            if attrib and ('k_attrib' not in node or attrib[0] not in node['k_attrib'] or (attrib[1] is not None and attrib[1] != node['k_attrib'][attrib[0]])):
                continue
            if value and ('k_value' not in node or value[0] not in node['k_value'] or (value[1] is not None and value[1] != node['k_value'][value[0]])):
                continue
            yield ntype, node

    def _substituteAtNode(self, n, subs, value):
        ''' substitute string in a given node '''
        if isinstance(value, str):
            value = escape(value)
        for d in ('k_value', 'k_attrib'):
            if d in n:
                for name in n[d]:
                    if isinstance(n[d][name], str) and subs in n[d][name]:
                        if isinstance(value, list):
                            # replace whole value with list
                            n[d][name] = value
                        else:
                            # just substring
                            n[d][name] = n[d][name].replace(subs, str(value))

    def substitute(self, marker, value):
        ''' from cursor substitute any substring values or attributes  %%marker%% with value
        '''
        subs = '%%{}%%'.format(marker)
        self._substituteAtNode(self.cursor, subs, value)
        for _, n in self.nodes():
            self._substituteAtNode(n, subs, value)

    def print(self, dirpath):
        ''' print as xml to default filename

            exiting file will be saved to .bak, existing bak destroyed

            path will be returned
        '''
        # print
        fpath = os.path.join(dirpath, KML_FILE)
        outputCheck(fpath, KML_FILE)

        with open(fpath, 'w') as kfile:
            kfile.write(XML_VERSION)
            pretty = '\n'
            self._printNode(kfile, 'kml', self.root['kml'][0], pretty)

        return fpath

    def _printNode(self, kfile, name, knode, pretty):
        ''' recursive node print '''
        if 'k_attrib' in knode:
            if len(knode) == 1:
                # only attributes in node
                kfile.write('{}<{} {}/>'.format(pretty, name, ' '.join(['{}="{}"'.format(k, str(knode['k_attrib'][k])) for k in knode['k_attrib']])))
                return
            else:
                kfile.write('{}<{} {}>'.format(pretty, name, ' '.join(['{}="{}"'.format(k, str(knode['k_attrib'][k])) for k in knode['k_attrib']])))
        else:
            kfile.write('{}<{}>'.format(pretty, name))
        newpretty = pretty + '    '
        if 'k_value' in knode:
            for n in knode['k_value']:
                v = knode['k_value'][n]
                if n == 'coordinates':
                    kfile.write('{0}<{1}>'.format(newpretty, n))
                    for t in v:
                        kfile.write('{}  {:.6f},{:.6f},{:d}'.format(pretty, t[1], t[0], 0))
                    kfile.write('{0}</{1}>'.format(newpretty, n))
                else:
                    kfile.write('{0}<{1}>{2}</{1}>'.format(newpretty, n, str(v)))
        for cname in knode:
            if cname not in ('k_attrib', 'k_value'):
                for cnode in knode[cname]:
                    self._printNode(kfile, cname, cnode, newpretty)
        kfile.write('{}</{}>'.format(pretty, name))
