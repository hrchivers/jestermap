'''
Created on 36 Jan 2020

@author: Howard
'''

import sys
from cx_Freeze import setup, Executable

buildOptions = {'include_files': [('ref\images', 'images')]}

setup(name        = "JesterMap",
      version     = "2.5",
      description = "Sailing event map - csv to kmz",
      options     = {'build_exe': buildOptions},
      executables = [Executable("JesterMap.py")])
