'''
Utilities for Jester Mapping

Created on 10 Jan 2020

@author: Howard Chivers
Copyright (c) 2020, Howard Chivers
All rights reserved.
See docs for licence

# position res
_dmsimmarsat = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<VD>[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9](?:\.[0-9]{1,7})?)[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9](?:\.[0-9]{1,7})?)[^\d]*", flags=re.IGNORECASE)
_degdecH     = re.compile("[^\d-]*(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d]))[^\d]*(?P<VH>north|south|n|s)[^\d-]*(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d]))[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_degdec      = re.compile("[^\d-]*(?P<VH>-)?(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7})[^\d-]*(?P<HH>-)?(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7})[^\d]*")
_demise      = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?:(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9](?:\.[0-9]{1,7})?))?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?:(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9](?:\.[0-9]{1,7})?))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_degmin      = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(:?(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?)?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(:?(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?)?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_degHmin     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]*(?P<VH>north|south|n|s)(?:[^\d]*(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?)?[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]*(?P<HH>east|west|e(?!st)|w)(:?[^\d]*(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?)?[^\d]*", flags=re.IGNORECASE)

# position res
# decimal degrees   without H decimal is required, otherwise decimal is optional     (H D [DD])
_deg      = re.compile("[^\d-]*(?P<VH>-)?(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d]))[^\d-]*(?P<HH>-)?(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d])[^\d]*)", flags=re.IGNORECASE)
_degH     = re.compile("[^\d-]*(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*(?P<VH>north|south|n|s)[^\d-]*(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_Hdeg     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d-]*(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d])?)[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d-]*(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*", flags=re.IGNORECASE)

# degrees and decimal minutes     must have both degrees and minutes, decimal fraction is optional   (H D M [MD])
_degminH     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_degHmin     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d]*(?P<VH>north|south|n|s)[^\d\.]*(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d\.]*(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?[^\d]*", flags=re.IGNORECASE)
_Hdegmin     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]*(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]*(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?[^\d]*", flags=re.IGNORECASE)

# degrees minutes and seconds     must have all three      (H D M S [SD])
_degminsecH     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9])(?:\"?(?P<VSD>\.[0-9]{1,7}))?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9])(?:\"?(?P<HSD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_Hdegminsec     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9])(?:\"?(?P<VSD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9])(?:\"?(?P<HSD>\.[0-9]{1,7}))?[^\d]*", flags=re.IGNORECASE)

'''

import logging
import re
import sys
import datetime
import traceback
import os
import locale

from include import DEBUG

# position res
# decimal degrees   without H decimal is required, otherwise decimal is optional     (H D [DD])
_deg      = re.compile("[^\d-]*(?P<VH>-)?(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d]))[^\d-]*(?P<HH>-)?(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d])[^\d]*)", flags=re.IGNORECASE)
_degH     = re.compile("[^\d-]*(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*(?P<VH>north|south|n|s)[^\d-]*(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_Hdeg     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d-]*(?P<VD>0?[0-8]?[0-9])°?(?P<VDD>\.[0-9]{1,7}(?![\"'’\d])?)[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d-]*(?P<HD>(?:3[0-5][0-9]|[0-2]?[0-9]?[0-9]))°?(?P<HDD>\.[0-9]{1,7}(?![\"'’\d]))?[^\d]*", flags=re.IGNORECASE)

# degrees and decimal minutes     must have both degrees (optional minutes (with optional decimal fractions of minutes)  (H D [M [MD]])
_degminH     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?:(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?)?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?:(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?)?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_degHmin     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d]*(?P<VH>north|south|n|s)[^\d\.]*(:?(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?)?[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d\.]*(?:(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?)?[^\d]*", flags=re.IGNORECASE)
_Hdegmin     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]*(?:(?P<VM>[0-5]?[0-9](?![\"°\d]))(?:(?:['’])?(?P<VMD>\.[0-9]{1,7}))?)?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]*(:?(?P<HM>[0-5]?[0-9](?![\"°]))(?:(?:['’])?(?P<HMD>\.[0-9]{1,7}))?)?[^\d]*", flags=re.IGNORECASE)

# degrees minutes and seconds     must have all three      (H D M S [SD])
_degminsecH     = re.compile("[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9])(?:\"?(?P<VSD>\.[0-9]{1,7}))?[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9])(?:\"?(?P<HSD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*", flags=re.IGNORECASE)
_Hdegminsec     = re.compile("[^\d]*(?P<VH>north|south|n|s)[^\d]*(?P<VD>0?[0-8]?[0-9](?![\"'’\d]))[^\d\.]+(?P<VM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<VS>[0-5]?[0-9])(?:\"?(?P<VSD>\.[0-9]{1,7}))?[^\d]*(?P<HH>east|west|e(?!st)|w)[^\d]*(?P<HD>3[0-5][0-9](?![\"'’\d])|[0-2]?[0-9]?[0-9](?![\"'’\d]))[^\d\.]+(?P<HM>[0-5]?[0-9](?![\"°\d]))[^\d\.]+(?P<HS>[0-5]?[0-9])(?:\"?(?P<HSD>\.[0-9]{1,7}))?[^\d]*", flags=re.IGNORECASE)

# accepts plain csv, or csv fields encased iun single or double quotes. embedded quotes of both types are possible, unless they are folloed by a comma. (Note this version uses non-greedy quantifiers and singel character look ahead)
_CSV_RE      = re.compile('\\s*(?:(?:"(.*?)")|(?:\'(.*?)\')|([^",\'].*?))??\\s*(?:,|$)')
_DATE_RE     = re.compile(r"[^\d]*([1-2][0-9]|[3][0-1]|0?[1-9])[^:\d]{1,5}(?:(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)|([1][0-2]|0?[1-9]))[^:\d]{1,10}((?:19|20)?[0-9]{2})\s*", flags=re.IGNORECASE)
_TIME_RE     = re.compile(r'(\d\d??):?(\d{2})(?::(\d{2})(?:\.(\d{1,6}))?)?')

_MONTH   = {'jan': 1, 'feb': 2, 'mar': 3, 'apr': 4, 'may': 5, 'jun': 6, 'jul': 7, 'aug': 8, 'sep': 9, 'oct': 10, 'nov': 11, 'dec': 12}
_TIME_FORMATS = {4: '%H%M', 5: '%H:%M', 6: '%H%M%S', 8: '%H:%M:%S', 15: '%H:%M:%S.%f'}
_THIS_YEAR = datetime.date.today().year - 2000

_utilLog = None

class InputError(ValueError):
    ''' Base class for module exceptions.

    Expects the first argument to be a message and the second, if present, to be the original Exception.
    Logs first two arguments as an error then passes exception to base Exception.
    '''
    def __init__(self, *args):
        log = args[0] + '(' + type(args[1]).__name__ + ': ' + str(args[1]) + ')' if len(args) > 1 else args[0]
        _utilLog.exception(log)
        if DEBUG:
            traceback.print_exc(file=sys.stdout)
        super().__init__(args[0])


def findData(filename):
    ''' see https://cx-freeze.readthedocs.io/en/latest/faq.html
        creates a path to a filename in same directory as script '''
    if getattr(sys, "frozen", False):
        # The application is frozen
        datadir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        datadir = os.path.dirname(__file__)
    return os.path.join(datadir, filename)


def _stringToTime(value):
    ''' string may be hmm hhmm hh:mm hhmmss hh:mm:ss hh:mm:ss.ssssss'''
    match = _TIME_RE.fullmatch(value)
    if not match:
        raise ValueError('incorrect time format ({})'.format(value))
    sec  = int(match.group(3)) if match.group(3) else 0
    usec = int(match.group(4)) * 10 ** (6 - len(match.group(4))) if match.group(4) else 0
    return datetime.time(int(match.group(1)), int(match.group(2)), sec, usec)


def _stringToDate(value):
    ''' accepts day-month-year with any separators and month as name or number and date as 4 or 2 digits'''
    match = _DATE_RE.fullmatch(value)
    if not match:
        raise ValueError('incorrect date format ({})'.format(value))
    year = int(match.group(4))
    if year < 1000:
        year = year + 1900 if year > _THIS_YEAR else year + 2000
    month = int(match.group(3)) if match.group(3) else _MONTH[match.group(2).lower()]
    day   = int(match.group(1))
    return datetime.date(year, month, day)


def parseLatLong(txt):
    ''' Convert lat-long string in any(?) format to decimal values

    returns lat, long (real)

    '''

    lat = 0.0
    lon = 0.0
    match = _degminsecH.fullmatch(txt)
    if not match:
        match = _Hdegminsec.fullmatch(txt)
    if match:
        lat = (float(match.group('VS')) + float(match.group('VSD'))) / 3600 if match.group('VSD') else float(match.group('VS')) / 3600
        lon = (float(match.group('HS')) + float(match.group('HSD'))) / 3600 if match.group('HSD') else float(match.group('HS')) / 3600

    if not match:
        match = _degminH.fullmatch(txt)
        if not match:
            match = _degHmin.fullmatch(txt)
        if not match:
            match = _Hdegmin.fullmatch(txt)

        if match and match.group('VMD'):
            lat += float(match.group('VMD')) / 60
        if match and match.group('HMD'):
            lon += float(match.group('HMD')) / 60

    if match and match.group('VM'):
        lat += float(match.group('VM')) / 60
    if match and match.group('HM'):
        lon += float(match.group('HM')) / 60

    if not match:
        match = _Hdeg.fullmatch(txt)
        if not match:
            match = _degH.fullmatch(txt)
        if not match:
            match = _deg.fullmatch(txt)

        if match and match.group('VDD'):
            lat += float(match.group('VDD'))
        if match and match.group('HDD'):
            lon += float(match.group('HDD'))

    if not match:
        raise ValueError('unable to parse position: {}'.format(txt))

    lat += float(match.group('VD'))
    lon += float(match.group('HD'))

    if match.group('VH') and match.group('VH').lower() in ('s', 'south', '-'):
        lat = -lat
    if match.group('HH') and match.group('HH').lower() in ('w', 'west', '-'):
        lon = -lon

    return lat, lon


def latlongToString(lat, long):
    ''' convert numerical floating lat long to a printable string
    '''
    if lat < 0:
        sign = 'S'
        lat = abs(lat)
    else:
        sign = 'N'
    deg = int(lat)
    res = "{:02d}\u00b0 {:06.3f}'{} ".format(deg, (lat - deg) * 60, sign)

    if long < 0:
        sign = 'W'
        long = abs(long)
    else:
        sign = 'E'
    deg = int(long)
    return res + " {:03d}\u00b0 {:06.3f}'{} ".format(deg, (long - deg) * 60, sign)


def boundsof(posns):
    ''' returns bounding box and center of a list of positions:
        (max lat, min lat, max lon, min lon , (lat, long))
    '''
    lat      = [p[0] for p in posns]
    max_lat  = max(lat)
    min_lat  = min(lat)
    long     = [p[1] for p in posns]
    max_long = max(long)
    min_long = min(long)
    return max_lat, min_lat, max_long, min_long, ((max_lat + min_lat) / 2, (max_long + min_long) / 2)


def outputCheck(fpath, name):
    ''' checks if a file exists, if so gives warning and changes file name to ...bak
        any existing .bak will be deleted
    '''
    if os.path.isfile(fpath):
        _utilLog.warning("Output file {} already exists, will be saved to '.bak'".format(name))
        fback = fpath + '.bak'
        if os.path.isfile(fback):
            os.remove(fback)
        os.rename(fpath, fback)


CSV_VALUE_TYPES = {'str': str,
                   'int': int,
                   'flost': float,
                   'date': _stringToDate,
                   'time': _stringToTime,
                   'position': parseLatLong}


def CSVRead(directory, filename, template, csv_encoding=locale.getpreferredencoding()):
    ''' Reads a CSV file using a Trec CSV Template

    Arguments:
        filpath (str):    Absolute filepath for input file
        template (list):  CSV import specification, see below

    templates are lists/tuples of (<columnname>, <key>, <required>, <type>)

    * *columnname* the column heading found in the top row.
    * *key* is the dict key assigned to the value,
    * *required* specifies that the field must be present,
    * *type* parameter is the datatype, see CSV_VALUE_TYPES

    All required columns must be present (otherwise raises InputError), columns not
    in the template are ignored. The order of input columns does not matter. The parser
    is tolerant of white space around separators and quotation marks.

    Yields:
         A row as a dict, empty values are missing from dict
    '''
    global _utilLog
    if not _utilLog:
        _utilLog = logging.getLogger('main.utilities')

    filepath = os.path.join(directory, filename)
    if not os.path.isfile(filepath):
        raise InputError('CSVRead - input path is not a file: {}'.format(filename))

    # build conversion dictionary {columnname: (key, required, convertfunction)}
    # convoluted map allows string forms of types in templates
    convert = {t[0]: (t[1], t[2], CSV_VALUE_TYPES[t[3]]) for t in template}

    with open(filepath, encoding=csv_encoding, errors='ignore') as csvfile:
        # get field headers
        try:
            line   = csvfile.readline().split(',')
            fields = [s.strip(' \r\n"').lower() for s in line]
        except Exception as e:
            raise InputError('CSVRead - read error when reading CSV header row from {}'.format(filename), e)

        lineno = 1
        while True:
            lineno += 1
            line    = csvfile.readline()
            if not line:
                break
            line = line.strip()
            if line == '':
                continue
            row = {}
            try:
                for i, match in enumerate(_CSV_RE.finditer(line)):
                    value = match.group(match.lastindex) if match.lastindex else ''
                    if value and value != '' and fields[i] in convert:
                        key, _ , converter = convert[fields[i]]
                        row[key]           = converter(value.strip())
            except Exception as e:
                _utilLog.error('Error reading {}, line {:d}: {}'.format(filename, lineno, str(e)))
                if DEBUG:
                    traceback.print_exc(file=sys.stdout)
                continue
            if not len(row):
                # blank line
                continue
            # check required values
            errors = []
            for t in template:
                if t[2] and t[1] not in row:
                    errors.append(t[0])
            if len(errors) > 0:
                _utilLog.error('Error reading {}, line {:d}, required columns missing: {}'.format(filename, lineno, ','.join(errors)))
                continue

            # then provide input
            yield(row)
