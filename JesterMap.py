'''
Created on 11 Jan 2020

@author: Howard Chivers

Copyright (c) 2020, Howard Chivers
All rights reserved.
See docs for licence


V2 May 2021, modified to work with My Maps (No overlay, some tidying, output to kmz file)
V2.1 June 2021
- Substitution list created to remove illegal characters from logs
- Allows early times without leading hours zero ( eg 700)
- Outputs most recent positions to log
- LOG_LAYER can be used to switch between placing all logs in a single folder v boat specific sub-folders
  NB Google My Maps will truncate after reading 10 folders, so default is single folder for all Logs
V2.2 Nov 2021
- removed doc.kml from final output
- kml files have dates added to filename
- boat names in 'jester_boats.csv' determine how they will be displayed, capitalisation of boats names in jester_tracks.csv can vary
- icons embedded in bin folder, command-line parameter for icon folder now optional
- status added to all position headers
- dot icons now used to mark positions with no log.
- xml escaping applied to all text fields (previously just log)
- 'location' changed to 'position'
- position formats: decimal seconds now parsed in DMS & inmarsat formats, all now case insensitive
V2.3 Jan 2022
- additional position formats (hemisphere after degrees), e.g. 12N 14'.5 45 W 12
  and decimal with hemispheres rather than signed, e.g. 12.4N 15.8W
- locale format input, with unicode optional switch
V2.4 Jan 2022
- additional formats (allow degree symbol embedded in decimal degrees: 41°.1333333N 22°.1166666W
- allow any non-decimal characters after position

Usage
JesterMap.exe [-u] <event folder>
-u switches input to UTF-8, normally not needed.

See docs for more detail on use.
'''

import sys
import os
import shutil
import logging
import datetime
import locale
from copy import deepcopy
from collections import OrderedDict

from include import TEMPLATE_TRACKS, TRACK_DEFAULTS, TEMPLATE_CONFIG, CONFIG_REQUIRED, CONFIG_DEFAULT, TEMPLATE_BOATS,\
    TRACK_FILE, CONFIG_FILE, BOAT_FILE, LOG_FILE, ICON_DIR,\
    KML_NAMESPACES, JESTER_STYLES, JESTER_STYLEMAP, COLOURS, JESTER_EVENT, JESTER_COURSE, EVENT_CONFIG,\
    TRACKS_ROOT, LOGS_ROOT, BOAT_PLACE, BOAT_FOLDER, LOG_PLACE, POINT_PLACE, LOG_LAYER
from utils import CSVRead, parseLatLong, boundsof, latlongToString, outputCheck, findData
from kml import Kml, getCamera


def abort(msg):
    print('FAILED: ' + msg)
    sys.exit(1)


def initLogging(dirpath):
    '''
    This sets up logging to both console and a log file in the working
    directory
    '''

    logging.basicConfig(filename=os.path.join(dirpath, LOG_FILE), level=logging.DEBUG, format="%(name)-12s %(levelname)-8s %(message)s")
    console = logging.StreamHandler(sys.stdout)
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)-12s %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    # say hullo
    startTime = datetime.datetime.now()
    logging.info("jester kvm writer started at " + startTime.strftime("%d-%m-%Y %H:%M"))


def readConfig(dirpath, csv_encoding):
    config = deepcopy(CONFIG_DEFAULT)
    for k in CSVRead(dirpath, CONFIG_FILE, TEMPLATE_CONFIG, csv_encoding):
        if k['name'] in ('start_end', 'finish_end', 'route_waymark'):
            try:
                config[k['name']].append(parseLatLong(k['value']))
            except Exception as _:
                logging.error('Unable to parse lat,long in config csv: {}'.format(k['value']))
        else:
            config[k['name']] = k['value']

    # start/finish/route lines - remove if no values, otherwise add to start and end to route
    for k in ('start_', 'finish_',):
        end = k + 'end'
        if not len(config[end]):
            del(config[end])
        else:
            config[k + 'point'] = boundsof(config[end])[4]
            if k == 'start_':
                config['route_waymark'].insert(0, config[k + 'point'])
            else:
                config['route_waymark'].append(config[k + 'point'])
    if len(config['route_waymark']) < 2:
        del(config['route_waymark'])

    for k in CONFIG_REQUIRED:
        if k not in config:
            abort('missing config value: {}'.format(k))
    return config


def readBoats(dirpath, csv_encoding):
    ''' colours are assigned as the boats are input to keep colours consistent when tracks are updated'''
    boats  = OrderedDict()
    colours = [k for k in sorted(COLOURS)]
    maxNameLength = 0
    for i, k in enumerate(CSVRead(dirpath, BOAT_FILE, TEMPLATE_BOATS, csv_encoding)):
        name = k['boat'].lower()
        if name in boats:
            logging.error('There are duplicate entries in {} for boat {}, the second entry will be ignored.'.format(BOAT_FILE, k['boat']))
            continue
        c = colours[i % len(colours)]
        boats[name] = {'displayname': k['boat'], 'sailor': k['sailor'], 'type': k['type'] if 'type' in k else '', 'colour': c}
        if len(k['boat']) > maxNameLength:
            maxNameLength = len(k['boat'])
    return boats, maxNameLength


def readTracks(dirpath, boats, csv_encoding):
    tracks = []
    for t in CSVRead(dirpath, TRACK_FILE, TEMPLATE_TRACKS, csv_encoding):
        for k in TRACK_DEFAULTS:
            if k not in t:
                t[k] = TRACK_DEFAULTS[k]
        t['boat'] = t['boat'].lower()
        if t['boat'] not in boats:
            logging.error('There is a track for boat {}, but this is not in the jester_boats.csv'.format(t['boat']))
            continue
        tracks.append(t)
    return tracks


if __name__ == '__main__':
    csv_encoding = locale.getpreferredencoding()
    if len(sys.argv) < 2:
        abort('missing argument, need event_directory')
    if len(sys.argv) == 3:
        dirpath = sys.argv[2]
        if sys.argv[1] == '-u':
            csv_encoding = 'UTF-8'
        else:
            abort("Argument 1 ({}) is invalid, the only valid flag is -u to select unicode (UTF-8) input format, \n perhaps your directory path has spaces and is not quoted?".format(sys.argv[1]))
    else:
        dirpath = sys.argv[1]
    if not os.path.isdir(dirpath):
        abort('Argument 1 ({}) is not a readable directory'.format(dirpath))

    # find path to icons
    iconpath = findData(ICON_DIR)
    if not os.path.isdir(iconpath):
        abort('images directory (icons) not found with executable file ({})'.format(iconpath))

    initLogging(dirpath)

    config                = readConfig(dirpath, csv_encoding)
    boats, maxNameLength  = readBoats(dirpath, csv_encoding)
    tracks                = readTracks(dirpath, boats, csv_encoding)

    # base kml and style sheets
    kml = Kml(KML_NAMESPACES)
    kml.addChild('Document', JESTER_EVENT, moveToChild=True)
    for s in JESTER_STYLES:
        kml.addChild('Style', s)
    for s in JESTER_STYLEMAP:
        kml.addChild('StyleMap', s)

    # event information
    kml.addChild('Folder', JESTER_COURSE, moveToChild=True)
    for trigger in EVENT_CONFIG:
        if trigger in config:
            kml.addChild('Placemark', EVENT_CONFIG[trigger])

    if len(tracks):

        # temp trees for tracks and logs
        k_tracks     = Kml(TRACKS_ROOT)
        k_logs       = Kml(LOGS_ROOT)
        currentposns = []
        logging.info('Most Recent Positions:')

        # collect boat info
        for boat in boats:
            # boat info
            meta = {'boat_name': boat, 'padded_name': boats[boat]['displayname'] + (' ' * (maxNameLength - len(boat)))}
            for k in boats[boat]:
                meta['boat_' + k] = boats[boat][k]

            # track
            log = [l for l in tracks if l['boat'] == boat]
            if not len(log):
                logging.info('No track information for boat: {}'.format(boats[boat]['displayname']))
                continue
            log = sorted(log, key=lambda x: (x['date'], x['time']))
            meta['boat_track'] = [r['position'] for r in log]
            meta['boat_track'].insert(0, config['start_point'])

            # last reported position
            for k in ('status', 'log'):
                meta['last_' + k] = log[-1][k] if k in log[-1] else ''
            meta['last_date'] = log[-1]['date'].strftime("%d/%m/%y")
            meta['last_time'] = log[-1]['time'].strftime("%H:%M")
            meta['last_position'] = [log[-1]['position']]
            logging.info('{}  {} {}  {}'.format(meta['padded_name'], meta['last_date'], meta['last_time'], latlongToString(*(meta['last_position'][0]))))
            currentposns += meta['last_position']

            # register track & position in temp folder
            k_tracks.addChild('Placemark', BOAT_PLACE)
            for k in meta:
                k_tracks.substitute(k, meta[k])

            # temp folder per boat for logs
            k_boat   = k_logs if LOG_LAYER else Kml(BOAT_FOLDER)

            # add each log place except last
            del(log[-1])
            for place in log:
                if place['log']:
                    k_boat.addChild('Placemark', LOG_PLACE)
                    k_boat.substitute('log_log', place['log'])
                else:
                    k_boat.addChild('Placemark', POINT_PLACE)
                k_boat.substitute('log_date', place['date'].strftime("%d/%m/%y"))
                k_boat.substitute('log_position', [place['position']])
                k_boat.substitute('log_status', place['status'])

            for k in ('boat_displayname', 'boat_colour'):
                k_boat.substitute(k, meta[k])

            if LOG_LAYER:
                # reset to root
                k_logs.setCursor(('kml',))
            else:
                # add to logs folder
                k_logs.addChild('Folder', k_boat.setCursor(('kml',)))

        # move folders to main tree
        kml.setCursor(('kml', 'Document'))
        kml.addChild('Folder', k_tracks.setCursor(('kml',)))
        kml.addChild('Folder', k_logs.setCursor(('kml',)))

        # set default camera view to current positions
        camera = getCamera(currentposns)
        kml.setCursor(('kml', 'Document'))
        kml.addChild('LookAt', camera.setCursor(('kml',)))

    # output
    try:
        kmz_name     = '{} {}.kmz'.format(config['event_name'], datetime.datetime.now().strftime("%Y-%m-%d %I%M"))
        kmz_filepath = os.path.join(dirpath, kmz_name)
        outputCheck(kmz_filepath, kmz_name)

        # make working directory
        temp_root = os.path.join(dirpath, 'temp_hrc_root_for_kmz_files')
        if os.path.isdir(temp_root):
            shutil.rmtree(temp_root)
        os.mkdir(temp_root)

        # make map file
        kml.setCursor(('kml',))
        for k in config:
            kml.substitute(k, config[k])
        kml.print(temp_root)

        # add icons & compress
        shutil.copytree(iconpath, os.path.join(temp_root, 'images'))
        shutil.make_archive(kmz_filepath, 'zip', temp_root)
        os.rename(kmz_filepath + '.zip', kmz_filepath)

        shutil.rmtree(temp_root)
    except Exception as e:
        logging.error('Failed to make kmz file: {}'.format(str(e)))

    logging.info('Done')
