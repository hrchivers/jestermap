This program is used to compile a track display for ocean sailing. The resulting map can show the positions, tracks and logs of a number of boats engaged in ocean cruising or racing. The input is a comma separated file of boat positions and logs, which can accept a wide range of formats for location and time allowing easy copying from position sources such as emails and other track displays. The output is a .kmz file that can be directly loaded into Google Earth or Google My Maps. 

This documentation descibes the use of the program and how to incorporate the resulting map display in a wordpress website.

This software is copyright but free to use, see licence terms including limitations of use and disclaimer below.

If you have any comments, problems or requests, please contact me, Howard Chivers, at howard@oddenhill.co.uk



