.. _ref_inputs:

===========
Input files
===========

The JesterMap program requires three csv (comma-separated) files as
inputs. Examples of these files can be found in the sample event.
It may be best to look at these first then using the detail below
to answer any outstanding questions.

The three files are:

.. tabularcolumns:: |l|l|


+---------------------+--------------------------------------------------------------------------------------------------------+
| *jester_config.csv* | Event information, such as name, start and finish and any waypoints required between start and finish. |
+---------------------+--------------------------------------------------------------------------------------------------------+
| *jester_boats.csv*  | List of entries: boat name, type, and skipper.                                                         |
+---------------------+--------------------------------------------------------------------------------------------------------+
| *jester_tracks.csv* | Position and log information.                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------+

The configuration and boats files are usually fixed for an event, and a new line added
to the tracks file for each position reported by a skipper.

CSV File format
---------------

CSV files are text files in which each line is divided into fields
(columns) by commas. Individual fields may be "double quoted" if
they contain commas. These files can be prepared using a spreadsheet
and then output in .csv format.

It is possible for spreadsheet programs to misinterpret entered data (e.g.
store a time as a date format, or a date as a simple number) resulting in
csv text that cannot be correctly interpreted. This will result in error
messages from JesterMap which give the line and the offending text. The
solution is to correct the entry in a text editor, or to correct the
spreadsheet cell format - if in doubt just format the whole column as text.

The fields in the first line of each file are column headers that
specify the content of each subsequent line. The columns may be
organised in any order. Some columns are required and some
optional (see below).

JesterMap's default input format matches the language setting used
on the computer. Extended character sets, such as characters in European
languages (e.g. è é ö) will be correctly interpreted and displayed. If you need
fully international characters then the spreadsheet needs to be saved as
*Unicode UTF-8* and the program run using the *-u* option.

jester_config.csv
-----------------

jester_config.csv is used to specific the event name, the start and finish lines and
the optimum race track. The file has only 3 columns which are used to provide named
configuration parameters and their values.:


+---------------------+---------------+-------------------------------------------------------------+
| Column              | Required?     | Content                                                     |
+=====================+===============+=============================================================+
| name                | Required      | Name of a configuration parameter                           |
+---------------------+---------------+-------------------------------------------------------------+
| value               | Required      | Value of the named parameter                                |
+---------------------+---------------+-------------------------------------------------------------+
| comment             | Optional      | Not used by JesterMap - it is for any explanations you      |
|                     |               | wish to add, e.g. reason for turning points on the route.   |
+---------------------+---------------+-------------------------------------------------------------+

Each line in the file allows the entry of one parameter. All the parameters are optional.
The possible parameters are:

+---------------------+---------------+-------------------------------------------------------------+
| Name                | Value Type    | Specifies                                                   |
+=====================+===============+=============================================================+
| event_name          | Text          | Short name of event.                                        |
+---------------------+---------------+-------------------------------------------------------------+
| event_description   | Text          | Longer (One sentence) event description                     |
+---------------------+---------------+-------------------------------------------------------------+
| start_description   | Text          | Brief description of start location.                        |
+---------------------+---------------+-------------------------------------------------------------+
| start_end           | Position      | The start or end of the start line.                         |
+---------------------+---------------+-------------------------------------------------------------+
| finish_description  | Text          | Brief description of end location.                          |
+---------------------+---------------+-------------------------------------------------------------+
| finish_end          | Position      | The start or end of the finish line                         |
+---------------------+---------------+-------------------------------------------------------------+
| route_waymark       | Position      | Intermediate points (between start and finish)              |
|                     |               | on the optimum route.                                       |
+---------------------+---------------+-------------------------------------------------------------+

See below for how positions and other values are written .

More than one value can be given for start_end, finish_end and route_waymark, each must be on a different
line. Normally the start line and finish line have two positions given, one at each end of the line.

The start and finish of the event are automatically added to the route, so if no route_waymarks are
specified then the route will be marked as a great circle between the centre of the start and finish
lines. If it is necessary to specify some turning points (e.g. to avoid crossing land) then each
intermediate waypoint should be provided in order from start to finish.

jester_boats.csv
----------------

jester_boats.csv is used to provide a list of the boats and skippers entered in the event.
Each is entered on a single line of the file, the columns are:

+-------------+------------+----------+-------------------------------------------------------------+
| Column      | Required?  | Type     | Content                                                     |
+=============+============+==========+=============================================================+
| boat        | Required   | Text     | Name of boat, must be unique.                               |
+-------------+------------+----------+-------------------------------------------------------------+
| sailor      | Required   | Text     | Name of Skipper                                             |
+-------------+------------+----------+-------------------------------------------------------------+
| type        | Optional   | Text     | Type of boat                                                |
+-------------+------------+----------+-------------------------------------------------------------+

The name of the boat will be displayed as it is written in this file. If
the boat name is capitalised differently in the track entries they will still match the correct boat.

jester_tracks.csv
-----------------

This file is used to record boat positions reported during the event. A single line is used for each
position and they can be recorded in any order.

A start position is automatically entered at the start for each boat (assuming the start line is given
in the jester_config.csv); however, an entry in this file is required for the finish position because not
all boats may cross the finish line.

The columns are:

+-------------+------------+----------+-------------------------------------------------------------+
| Column      | Required?  | Type     | Content                                                     |
+=============+============+==========+=============================================================+
| boat        | Required   | Text     | Name of boat, must be registered in jester_boats.csv        |
+-------------+------------+----------+-------------------------------------------------------------+
| date        | Required   | Date     | Date of log.                                                |
+-------------+------------+----------+-------------------------------------------------------------+
| time        | Optional   | Time     | Time of log, if not entered 12:00 is the default.           |
+-------------+------------+----------+-------------------------------------------------------------+
| position    | Required   | Position | Position of boat.                                           |
+-------------+------------+----------+-------------------------------------------------------------+
| status      | Optional   | Text     | Short additional information, see below.                    |
+-------------+------------+----------+-------------------------------------------------------------+
| log         | Optional   | Text     | Text log, may be short paragraph but must be                |
|             |            |          | entered on a single line.                                   |
+-------------+------------+----------+-------------------------------------------------------------+

Time is not displayed in text on the map, but is used to sort entries for a boat if several positions
are entered on the same day.

Any *status* text will be added to the title of each log and was intended to be a single word, such as *Finished*,
*Retired* or perhaps *becalmed*. It could be used to manually display time if several entries are provided
on the same day.

The log field may accommodate a paragraph of text, or be completely absent. Position reports with logs are
marked with a different icon to those without.

Input Formats
-------------

As far as possible a wide range of input formats are supported, with the aim of
allowing data from various sources to be cut and paste directly into the various
input files, especially the tracks file, without the need for reformatting or
conversion.

.. note::

    If the program reports a formatting error, remember that it may have being
    attempting to convert the wrong data because of an issue with the columns
    in the input file. This should be obvious if the data reported in the error
    message is of the wrong type (e.g. failure to convert time when the data quoted
    is a date.)

A brief guide to the options with examples is given below.

Text
====

Text inputs may not include new line characters; they may include commas,
in which case they must be enclosed in single or double quotation marks
*"like, this"*. quotation marks within a field are allowed but might be
mistaken for the closing quotation if they are followed by a comma.

If using Microsoft Excel to prepare the .csv file, text fields will be
automatically enclosed in double quotes. If a line does throw an error
it may be that the problem is that there are unquoted commas in the line
which confuse the program about which field is in which column.

Date
====

Dates must have three elements separated by any characters except *':'*.

+----------------------+-----------------------------------------------------------------------+
| Examples             | Comment                                                               |
+======================+=======================================================================+
| 05/3/21              | Years given as two digits will be interpreted in current year or the  |
+----------------------+ past (e.g. in 2021 a year of 22 would be converted to 1922, in 2022 it|
| 5/03/2021            | would be converted to 2022.                                           |
+----------------------+                                                                       |
| 5 Mar 21             | Months may be named in short or long form with any capitalisation.    |
+----------------------+                                                                       |
| 05 march 2021        |                                                                       |
+----------------------+-----------------------------------------------------------------------+

Time
====

Time may be hours and minutes with an optional separator which must be a colon. Seconds and
decimal fractions of a second are both optional, and are probably not needed for most applications.

+----------------------+-----------------------------------------------------------------------+
| Examples             | Comment                                                               |
+======================+=======================================================================+
| 935                  |                                                                       |
+----------------------+ Only *':'* can be used as a separator.                                |
| 0935                 |                                                                       |
+----------------------+                                                                       |
| 11:35                |                                                                       |
+----------------------+ Seconds must always be preceded by a separator.                       |
| 935:40               |                                                                       |
+----------------------+                                                                       |
| 11:35:40.123456      |                                                                       |
+----------------------+-----------------------------------------------------------------------+

Position
========

There are 3 main types of position format; in every format Latitude must be first and Longitude
second. Within the 4 types there is considerable flexibility, in most cases any separators can be used
(e.g between degrees and minutes) and the capitalisation of words (e.g. north, North) is irrelevant.
Other text (e.g. *Lat* or *Lon* is ignored where possible.

+--------------------------------------+-----------------------------------------------------------------------+
| Examples                             | Format                                                                |
+======================================+=======================================================================+
| "42°50'3" N, 100-42-12 W"            | Degrees Minutes Seconds (optional decimal seconds)                    |
+--------------------------------------+                                                                       |
| 43°50'3.6 S 102°42'12.21 East        |                                                                       |
+--------------------------------------+                                                                       |
| Lat: N 42 50' 3" Lon: W 100 42' 12"  | Hemisphere (N,S, E, or W etc) can also be placed before the positions.|
+--------------------------------------+                                                                       |
| s 43 50' 3.6" e 102 42' 12.21"       |                                                                       |
+--------------------------------------+-----------------------------------------------------------------------+
| 42 50.05 North 100 42.2 w            | Degrees Minutes (optional fractions of minutes)                       |
+--------------------------------------+                                                                       |
| 43 50.06 south 102 42'.2035 E        |                                                                       |
+--------------------------------------+                                                                       |
| 42 N 50.05 100 w 42.2                | Hemisphere may also be specified between or before                    |
+--------------------------------------+ degrees and minutes.                                                  |
| Lat: N 42°50.05 Lon: W 100°42.2      |                                                                       |
+--------------------------------------+-----------------------------------------------------------------------+
| 42.834166 -100.703333                | Degrees.decimal fractions of degrees. The default hemispheres are N   |
+--------------------------------------+ and E; South and West may be indicated by minus signs.                |
| -43°.834333 102°.703392              |                                                                       |
+--------------------------------------+ Embedded degree symbols may be used before the decimal points.        |
| "-43.834333, 102.703392"             |                                                                       |
+--------------------------------------+                                                                       |
| 43.834 S 102.7E                      | The usual hemisphere letters/words may be used instead of '-' signs.  |
+--------------------------------------+                                                                       |
| N 43.834 W 102.7                     |                                                                       |
+--------------------------------------+-----------------------------------------------------------------------+

Note that the program requires the same format to be used for both latitude and longitude within a single position.
So, for example, a position of *43°50'3.6 S -100.703333* or any other combination of the main formats would be
reported as an error.

Another possible error is that positions which include a comma between latitude and longitude need to be placed
between quotes to avoid confusion about the number of columns in the csv file. (In Excel, format the position
column as text.)
