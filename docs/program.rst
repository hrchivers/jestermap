.. _ref_program:

=================
JesterMap Program
=================

Distribution Package
--------------------

JesterMap can only run on a Microsoft Windows system. It is distributed as a .zip archive
with the following content:

.. tabularcolumns:: |l|l|

+-------------------+-------------------------------------------------------------------------------+
| *bin*             | Folder contains the program (*JesterMap.exe*) and supporting files.           |
+-------------------+-------------------------------------------------------------------------------+
| *Sample*          | Sample event, with both input and output files.                               |
+-------------------+-------------------------------------------------------------------------------+
| *html*            | Documentation (open *index.html* for documentation).                          |
+-------------------+-------------------------------------------------------------------------------+
| *RunJesterMap.bat*| Sample batch file for running the program.                                    |
+-------------------+-------------------------------------------------------------------------------+

The archive should be unzipped and placed in a convenient folder for use.
The progam does not need to be installed.

Event files are best placed in a separate folder.


Running JesterMap
-----------------


To run the program execute the command:

    JesterMap.exe [-u] <event folder>

Where *<bin folder>* is the path to the executable and the *<event folder>* is a
path to the folder containing the three event files. The option *-u* is not normally needed
and can be omitted, see below.

For example, if the event files are in C:/Event then the command would be:

    *JesterMap.exe C:/Event*

..  note::
    Since this is not an 'installed' program you will need to add JesterMap.exe
    to the command path, or give the full path to the program,
    e.g. *C:/Users/Me/Program Files/JesterMap/bin/JesterMap.exe*.
    If the path to the event folder has embedded spaces it must be enclosed in quotes.

The *RunJesterMap.bat* file may be a convenient way of running the program - edit it
to include the correct path for the program. The event
folder can then be dragged and dropped onto *RunJesterMap.bat* to create a map file.

The output map file is named *<event name><date & time>.kmz* and is created in the
event folder.  The event name is specified in the event configuration (see below)
and the date and time in the file name avoids overwriting previous maps.
This file can be opened directly in Google Earth or in Google My Maps. The necessary
icons are embedded within the file.

The program also writes a log to the console, and appends the same information
to a *jester.log* file in the event directory. The log from every run includes
the most recent recorded position for every boat:

..  image:: figures/output_log.jpg

The log will warn if there are positions recorded for boats which are not in the boats
configuration file, or if boats present in the configuration have not reported
any positions, as above.

The log will also warn if any fields (e.g. date, position) cannot be decoded,
together with the number of the line with the error.
In the following example, longitude is missing in the position:

..  image:: figures/JM_error.jpg

Lines with errors of this sort are discarded so the log should always be checked
to ensure there are no such errors. Format errors may be caused by formatting inserted
or modified by a spreadsheet program; it can sometimes be necessary to correct
such errors in a basic text editor. See below for more information about accepted
formats.

Option -u
.........

This option switches the input format from the normal local Windows encoding to Unicode UTF-8.

Normally this option is not required, and should be avoided if the input files are created using
Excel. If the input files are prepared on a non-Windows computer (Linux or Mac) then they should be
saved as UTF-8 and can then be read using the *-u* option.

The other case in which Unicode input is justified is if a wide range of international characters
is required. The default windows encoding is able to accommodate most European characters and symbols
such as degree, but if a wider set of international characters is required then the input files need
to be saved and read as UTF-8.
