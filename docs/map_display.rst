.. _ref_display:

==================
Displaying the Map
==================

The .kml file produced by JesterMap can be displayed (and tested) simply 
by opening it using the desktop *Google Earth* application. This section will
describe using Google MyMaps to display the map, allowing it to be shared over 
the Internet and added to websites.

The advantage of using MyMaps is that it requires only a normal Google user account 
- it does not require a cloud account or an application programming (API) key. 
MyMaps can also be added to a website without the need for the website to support 
the Google map API, so there is no requirement for special plugins to display 
the map on a wordpress site.
 
The disadvantage to MyMaps is that they are limited in the types of display that 
can be drawn. Some elements of the map are ignored (such as track highlighting
which can be seen on Google Earth). JesterMap avoids using map elements that
cause errors in MyMaps. 


Display Using MyMaps
********************

You will need a free Google account. If in doubt register for a gmail email and
this automatically creates a Google account with access to other Google services.

MyMaps can be access as follows:

    * Open your Google acount (e.g. gmail) then open **Maps**.
    
     ..  image:: figures/SelectgMaps_small.jpg
 
    * In Maps click the Menu **Your places**, then select **Maps**.

From here you can select a map to edit, or create a new map.    
    
..  image:: figures/NewMap_small.jpg
    
Set the title of the new map and an optional description by clicking on the title, 
and also set the base map. (The 'satellite' base map has ocean features such as 
the continental shelf.)

Select 'import' and either drag and drop the .kml file into the window, or use the 
button to naviagte to the file on your computer.

The map will now open in MyMaps. Note that you can edit the map, so, for example, 
be careful not to drag log positions off the track while viewing. (Public viewers
don't have this superpower.) 

Updating the map
****************

When a new set of positions have been reported and a new map created with the 
JesterMap program, it is necessary to update the map in MyMaps. 

**Do not delete the whole map and create a new one.** The problem with deleting 
the whole map is that if you have made the map public by placing a reference to 
it on a website (see below) then a new map would have a new reference and any 
links to the map would then need to be updated. Instead, delete just the map 
layers, leaving the title and then reload within the existing map. That way 
any public links to the map do not need to be changed.

JesterMap organises the map in three layers:

* Event Information (start, finish and route)
* Tracks (the line followed by each boat)
* Logs (all logs and waypoints) 

(JesterMap uses three layers rather than a layer for each boat to avoid hitting 
element limits in MyMaps, however, limiting the number of layers also proves 
convenient for updating the map.)  

To delete all the elements of the map, delete the three layers in turn by
selecting the menu to the right of the layer name then *Delete this layer*. 

..  image:: figures/DeleteEventInfo_small.jpg

The new map can then be uploaded as before.

Posting the map on Wordpress
****************************

MyMaps can be shown on other websites via an iFrame. (An iFrame puts a web page within
another web page, so viewers see the map page embedded within a page on your website.)

First it is necessary to allow others to see your map. While in your map on MyMaps, 
select 'Share' and adjust the settings to enable:

* Anyone with this link can view
* Let others search for and find this map on the internet. 

..  image:: figures/ShareMap_small.jpg

Next obtain the link which is to be embedded on your website. From the map menu
select *Embed on my site*, copy and paste the whole of the text provided 
(<iframe ...>) into a text file. This is the link that will be placed on your website.

..  image:: figures/EmbedLink_small.jpg

On the wordpress page that you are constructing or editing, insert a *Custom html* block. 
If necessary use 'html' as a search term to find this block in the list of blocks that 
can be added.

Paste the text text saved from the map into the custom html block, that is all that is needed.

..  image:: figures/embed_iframe_small.jpg

You can use *preview* within the block menu to check that it correctly connects to the map.
Note that after one connection the actual html may be updated, but it will still work.

If you need to adjust the size of the inserted frame when the whole wordpress page is previewed,
the frame size is controlled by the width and height parameters (*width="640" height="480"* in the example). 
These can be adjusted to suit your page layout. 

Remember to publish or update the page to make the map public.


 
