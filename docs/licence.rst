=======
Licence
=======

Copyright (c) 2019-2021, Howard Chivers
All rights reserved.

Software Licence
****************
This software is not suitable and should not be used for any form of safety 
monitoring or management. 

The software may be used as a stand-alone package, or incorporated into a 
workflow by scripting or other means.

Redistribution and use of this software is permitted provided that the following 
conditions are met:

* The software package is used as provided; no part of the software may be 
  separately reused or incorporated in another program.
* The software may not be incorporated in a commercial product.
* Redistribution of this software must retain the above copyright notice, 
  these conditions and restrictons and the following disclaimer:

Disclaimer
**********
This software is provided 'as is' and any express or implied warranties, 
including but not limited to the implied warranty of fitness for purpose, 
are disclaimed. In no event shall the originator, other contributors or their 
employers be liable for any direct, indirect, incidental, special, exemplary, 
or consequential damages, however caused and on any theory of liability, 
arising out of the use of this software.


